#include "hyperdual.hpp"
#include <cmath>
#include <iostream>

template <typename number>
void f_out(const vec<number, 6>& in, vec<number, 4>& out)
{
    double d = 1.0;
    double h = 0.1;

    const number& x = in[0];
    const number& y = in[1];
    const number& th = in[2];
    const number& v = in[3];

    const number& w = in[4];
    const number& a = in[5];

    number f_ = h * v;
    number cw = cos(w);
    number b = f_ * cw + d - sqrt(d * d - f_ * f_ * (1.0 - cw * cw));

    number c = cos(th);
    number s = sin(th);

    out[0] = x + b * c;
    out[1] = y + b * s;
    out[2] = th + asin(sin(w) * f_ / d);
    out[3] = v + h * a;
}

template <typename number> vec<number, 4> f(vec<number, 6>& in)
{
    vec<number, 4> out;
    f_out(in, out);
    return out;
}

int main()
{
    const double eps = 1e-6;
    vec<double, 6> in;
    vec<double, 4> out;

    in <<
        // state
        0.5488135039273248,
        0.7151893663724195, 0.6027633760716439, 0.5448831829968969,
        // control
        0.4236547993389047, 0.6458941130666561;
    f_out(in, out);

    vec<double, 6> dx = eps * vec<double, 6>::Random();
    vec<double, 6> in_ = in + dx;

    auto dual = from_vector<6>(in);
    auto res = f(dual);

    vec<double, 4> j;
    for (long i = 0; i < 4; ++i)
    {
        j[i] = res[i].b.transpose() * dx;
    }

    auto df = f(in_) - f(in);
    auto ddf = f(in_) - f(in) - j;
    std::cout << (ddf.transpose()) << std::endl;
    std::cout << (ddf.transpose()) / eps << std::endl;
    std::cout << std::endl;

    std::cout << (df.transpose()) / eps << std::endl;
    for (long i = 0; i < 4; ++i)
    {
        std::cout << (res[i].b.transpose() * dx) / eps << ", ";
    }
    std::cout << std::endl;

    std::cout << (ddf.transpose()) / eps / eps << std::endl;
    for (long i = 0; i < 4; ++i)
    {
        std::cout << .5 *
                         (dx.transpose() *
                          ((res[i].c + res[i].c.transpose()) * dx)) /
                         eps / eps
                  << ", ";
    }
    std::cout << std::endl;
}

#include "helper.hpp"
#include "hyperdual.hpp"
#include <cmath>

constexpr double d = 1.0;
constexpr double h = 0.1;

constexpr double p_x = 0.1;
constexpr double p_y = p_x;
constexpr double p_th = 0.01;
constexpr double p_v = 1.0;
constexpr double c_w = 1e-2;
constexpr double c_a = 1e-4;

template <typename Derived>
using Scalar = typename Eigen::MatrixBase<Derived>::Scalar;

template <typename Derived>
void system_f(const Eigen::MatrixBase<Derived>& in, Scalar<Derived>* out)
{
    using Scalar = Scalar<Derived>;
    const Scalar& x = in[0];
    const Scalar& y = in[1];
    const Scalar& th = in[2];
    const Scalar& v = in[3];

    const Scalar& w = in[4];
    const Scalar& a = in[5];

    const Scalar f_ = h * v;
    const Scalar cw = cos(w);
    const Scalar b = f_ * cw + d - sqrt(d * d - f_ * f_ * (1.0 - cw * cw));

    const Scalar c = cos(th);
    const Scalar s = sin(th);

    out[0] = x + b * c;
    out[1] = y + b * s;
    out[2] = th + asin(sin(w) * f_ / d);
    out[3] = v + h * a;
}

template <typename Scalar> Scalar z(Scalar x, double p)
{
    return sqrt(x * x + p * p) - p;
}

template <typename Derived>
Scalar<Derived> system_l(const Eigen::MatrixBase<Derived>& in)
{
    using Scalar = Scalar<Derived>;
    const Scalar& x = in[0];
    const Scalar& y = in[1];
    const Scalar& w = in[4];
    const Scalar& a = in[5];

    return 0.01 * (z(x, p_x) + z(y, p_y)) + c_w * w * w + c_a * a * a;
}

template <typename Derived>
Scalar<Derived> system_lf(const Eigen::MatrixBase<Derived>& in)
{
    using Scalar = Scalar<Derived>;
    const Scalar& x = in[0];
    const Scalar& y = in[1];
    const Scalar& th = in[2];
    const Scalar& v = in[3];

    return z(x, p_x) + z(y, p_y) + z(th, p_th) + z(v, p_v);
}

GENERATE_MODULE(car, 4, 2)

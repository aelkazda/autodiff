#include "helper.hpp"
#include "hyperdual.hpp"
#include <cmath>

constexpr double d = 1.0;
constexpr double h = 0.1;

constexpr double p_x = 0.1;
constexpr double p_y = p_x;
constexpr double p_th = 0.01;
constexpr double p_v = 1.0;
constexpr double c_w = 1e-2;
constexpr double c_a = 1e-4;

constexpr int INTEGRATION_ORDER = 6;
constexpr std::array<double, INTEGRATION_ORDER> POINTS = {
    0.033765242898423975, 0.16939530676686776, 0.3806904069584015,
    0.6193095930415985,   0.8306046932331322,  0.966234757101576};
constexpr std::array<double, INTEGRATION_ORDER> WEIGHTS = {
    0.08566224618958511, 0.18038078652406928, 0.23395696728634566,
    0.23395696728634566, 0.18038078652406928, 0.08566224618958511};

template <typename Derived>
using Scalar = typename Eigen::MatrixBase<Derived>::Scalar;

template <typename Scalar> inline Scalar z(const Scalar& x, double p)
{
    return sqrt(x * x + p * p) - p;
}

template <typename Scalar>
inline Scalar theta(const Scalar& th, const Scalar& c1, const Scalar& c2,
                    const double dt)
{
    return th + dt * c1 + (dt * dt) * c2;
}

template <typename Scalar>
inline Scalar x_dot(const Scalar& th, const Scalar& v)
{
    return v * cos(th);
}

template <typename Scalar>
inline Scalar y_dot(const Scalar& th, const Scalar& v)
{
    return v * sin(th);
}

template <typename Derived>
void system_f(const Eigen::MatrixBase<Derived>& in, Scalar<Derived>* out)
{
    using Scalar = Scalar<Derived>;
    const Scalar& x = in[0];
    const Scalar& y = in[1];
    const Scalar& th = in[2];
    const Scalar& v = in[3];

    const Scalar& w = in[4];
    const Scalar& a = in[5];

    const Scalar c1 = tan(w) / d;
    const Scalar c2 = 0.5 * a * tan(w) / d;

    out[2] = theta(th, c1, c2, h);
    out[3] = v + h * a;

    out[0] = x;
    out[1] = y;
    for (int i = 0; i < INTEGRATION_ORDER; ++i)
    {
        double dt = h * POINTS[static_cast<size_t>(i)];
        double weight = WEIGHTS[static_cast<size_t>(i)];

        Scalar th_dt = theta(th, v, a, h * dt);
        Scalar v_dt = v + dt * a;

        out[0] += weight * x_dot(th_dt, v_dt);
        out[1] += weight * y_dot(th_dt, v_dt);
    }
}

template <typename Derived>
Scalar<Derived> system_l(const Eigen::MatrixBase<Derived>& in)
{
    using Scalar = Scalar<Derived>;
    const Scalar& x = in[0];
    const Scalar& y = in[1];
    const Scalar& w = in[4];
    const Scalar& a = in[5];

    return 0.01 * (z(x, p_x) + z(y, p_y)) + c_w * w * w + c_a * a * a;
}

template <typename Derived>
Scalar<Derived> system_lf(const Eigen::MatrixBase<Derived>& in)
{
    using Scalar = Scalar<Derived>;
    const Scalar& x = in[0];
    const Scalar& y = in[1];
    const Scalar& th = in[2];
    const Scalar& v = in[3];

    return z(x, p_x) + z(y, p_y) + z(th, p_th) + z(v, p_v);
}

GENERATE_MODULE(carv2, 4, 2)

#ifndef DERIVATIVES
#define DERIVATIVES

#include <Eigen/Dense>
#include <cmath>

template <double (*f)(double)> Eigen::Vector3d derivative(double in);
Eigen::Vector3d reciprocal_derivative(double in);

#endif /* ifndef DERIVATIVES */

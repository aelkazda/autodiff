#ifndef HYPERDUAL
#define HYPERDUAL

#include "derivatives.hpp"
#include <Eigen/Dense>
#include <cmath>
#include <iostream>
#include <iterator>

#define EXTEND(function)                                                       \
    template <long n> hyperdual<n> function(hyperdual<n> in)                   \
    {                                                                          \
        vec<double, 3> f_derivatives = derivative<function>(in.a);             \
        in.c *= f_derivatives[1];                                              \
        in.c += ((f_derivatives[2] / 2.0) * in.b) * in.b.transpose();          \
        in.b *= f_derivatives[1];                                              \
        in.a = f_derivatives[0];                                               \
        return in;                                                             \
    }

template <typename number, long n> using vec = Eigen::Matrix<number, n, 1>;
template <typename number, long n> using mat = Eigen::Matrix<number, n, n>;

template <long n> struct hyperdual
{
    // Standard part
    double a;
    // Dual part
    vec<double, n> b;
    // Hyperdual part
    mat<double, n> c;

    hyperdual<n>()
    {
    }

    hyperdual<n>(const double standard, long i)
    {
        this->a = standard;
        this->b.setZero();
        this->c.setZero();
        this->b(i) = 1.0;
    }

    hyperdual<n>(const double standard, const vec<double, n>& dual,
                 const mat<double, n>& dual2)
    {
        this->a = standard;
        this->b << dual;
        this->c << dual2;
    }

    hyperdual<n>(const double standard, const vec<double, n>& dual)
    {
        this->a = standard;
        this->b << dual;
        this->c.setZero();
    }

    hyperdual<n>(const double standard)
    {
        this->a = standard;
        this->b.setZero();
        this->c.setZero();
    }
};

// Plus
template <long n>
hyperdual<n>& operator+=(hyperdual<n>& lhs, const hyperdual<n>& rhs)
{
    lhs.a += rhs.a;
    lhs.b += rhs.b;
    lhs.c += rhs.c;
    return lhs;
}

template <long n>
hyperdual<n> operator+(hyperdual<n> lhs, const hyperdual<n>& rhs)
{
    lhs += rhs;
    return lhs;
}

template <long n> hyperdual<n> operator+(hyperdual<n> lhs, const double& rhs)
{
    lhs.a += rhs;
    return lhs;
}
template <long n>
hyperdual<n> operator+(const double& lhs, const hyperdual<n> rhs)
{
    return rhs + lhs;
}

// Minus
template <long n>
hyperdual<n>& operator-=(hyperdual<n>& lhs, const hyperdual<n>& rhs)
{
    lhs.a -= rhs.a;
    lhs.b -= rhs.b;
    lhs.c -= rhs.c;
    return lhs;
}

template <long n>
hyperdual<n> operator-(hyperdual<n> lhs, const hyperdual<n>& rhs)
{
    lhs -= rhs;
    return lhs;
}

template <long n> hyperdual<n> operator-(hyperdual<n> lhs, const double& rhs)
{
    lhs.a -= rhs;
    return lhs;
}
template <long n> hyperdual<n> operator-(const double& lhs, hyperdual<n> rhs)
{
    rhs.a = lhs - rhs.a;
    rhs.b = -rhs.b;
    rhs.c = -rhs.c;
    return rhs;
}

// Product
template <long n>
hyperdual<n>& operator*=(hyperdual<n>& lhs, const hyperdual<n>& rhs)
{
    lhs.c *= rhs.a;
    lhs.c += lhs.a * rhs.c;
    lhs.c += lhs.b * rhs.b.transpose();

    lhs.b *= rhs.a;
    lhs.b += lhs.a * rhs.b;

    lhs.a *= rhs.a;
    return lhs;
}

template <long n>
hyperdual<n> operator*(hyperdual<n> lhs, const hyperdual<n>& rhs)
{
    lhs *= rhs;
    return lhs;
}

template <long n> hyperdual<n> operator*=(hyperdual<n>& lhs, const double& rhs)
{
    lhs.a *= rhs;
    lhs.b *= rhs;
    lhs.c *= rhs;
    return lhs;
}
template <long n> hyperdual<n> operator*(hyperdual<n> lhs, const double& rhs)
{
    lhs *= rhs;
    return lhs;
}
template <long n>
hyperdual<n> operator*(const double lhs, const hyperdual<n>& rhs)
{
    return rhs * lhs;
}

// Division
template <long n> hyperdual<n> reciprocal(hyperdual<n> in)
{
    vec<double, 3> derivatives = reciprocal_derivative(in.a);
    in.c *= derivatives[1];
    in.c += ((derivatives[2] / 2.0) * in.b) * in.b.transpose();
    in.b *= derivatives[1];
    in.a = derivatives[0];
    return in;
}

template <long n>
hyperdual<n> operator/(hyperdual<n> lhs, const hyperdual<n>& rhs)
{
    lhs *= reciprocal(rhs);
    return lhs;
}

template <long n> hyperdual<n> operator/(hyperdual<n> lhs, const double rhs)
{
    lhs *= 1.0 / rhs;
    return lhs;
}

template <long n>
hyperdual<n> operator/(const double lhs, const hyperdual<n>& rhs)
{
    hyperdual<n> out = reciprocal(rhs);
    out *= lhs;
    return out;
}

// Vector to hyperdual vector
template <long n> vec<hyperdual<n>, n> from_vector(vec<double, n> in)
{
    vec<hyperdual<n>, n> out;
    for (long i = 0; i < n; ++i)
    {
        out[i] = hyperdual<n>(in[i], i);
    }
    return out;
}

template <long n> vec<hyperdual<n>, n> from_vector(const double* in)
{
    vec<hyperdual<n>, n> out;
    for (long i = 0; i < n; ++i)
    {
        out[i] = hyperdual<n>(in[i], i);
    }
    return out;
}

// Prlong
template <long n>
std::ostream& operator<<(std::ostream& out, const hyperdual<n>& arr)
{
    std::cout << "[" << std::endl;
    std::cout << arr.a << "," << std::endl;
    std::cout << arr.b.transpose() << "," << std::endl;
    std::cout << (arr.c + arr.c.transpose()) << std::endl;
    std::cout << "]";
    return out;
}

template <long n>
std::ostream& operator<<(std::ostream& out, const vec<hyperdual<n>, n>& arr)
{
    std::cout << "[" << std::endl;
    for (long i = 0; i < n; ++i)
    {
        std::cout << arr[i] << std::endl;
    }
    std::cout << "]";
    return out;
}

// Scalar functions
EXTEND(sin)
EXTEND(asin)
EXTEND(cos)
EXTEND(tan)
EXTEND(sqrt)

#endif /* ifndef HYPERDUAL */

#ifndef HELPER
#define HELPER

#include "hyperdual.hpp"
#include <pybind11/eigen.h>
#include <pybind11/numpy.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;
using ndarray = py::array_t<double, py::array::c_style>;

template <long n> using Vector = Eigen::Matrix<double, n, 1>;
template <long n_rows, long n_cols>
using ColMatrix = Eigen::Matrix<double, n_rows, n_cols, Eigen::ColMajor>;
template <long n_rows, long n_cols>
using RowMatrix = Eigen::Matrix<double, n_rows, n_cols, Eigen::RowMajor>;
using Eigen::Map;

void compute_states(const ndarray controls, ndarray states);

std::tuple<ndarray, ndarray> batch_f(const ndarray in);
std::tuple<ndarray, ndarray, ndarray, ndarray> batch_l(const ndarray in);

template <long in_size, long out_size>
inline void get_derivatives(hyperdual<in_size>* f_out, double* j_out,
                            double* h_out, const long index)
{
    for (long j = 0; j < out_size; ++j)
    {
        const double* j_data = f_out[j].b.data();
        std::copy(j_data, j_data + in_size,
                  j_out + (out_size * in_size * index + in_size * j));

        f_out[j].c += (f_out[j].c.transpose()).eval();

        const double* h_data = f_out[j].c.data();
        std::copy(h_data, h_data + in_size * in_size,
                  h_out + (out_size * in_size * in_size * index +
                           in_size * in_size * j));
    }
}

template <long in_size>
inline void get_derivatives(hyperdual<in_size>& f_out, double* j_out,
                            double* h_out, const long index)
{
    const double* j_data = f_out.b.data();
    std::copy(j_data, j_data + in_size, j_out + in_size * index);

    f_out.c += (f_out.c.transpose()).eval();

    const double* h_data = f_out.c.data();
    std::copy(h_data, h_data + in_size * in_size,
              h_out + in_size * in_size * index);
}

#define GENERATE_MODULE(module_name, n_x, n_u)                                 \
    void compute_states(const ndarray initial_state, ndarray traj)             \
    {                                                                          \
        auto n = traj.shape(0) - 1;                                            \
        auto traj_ptr = traj.mutable_data(0);                                  \
        auto init_ptr = initial_state.data(0);                                 \
                                                                               \
        std::copy(init_ptr, init_ptr + n_x, traj_ptr);                         \
        for (long i = 0; i < n; ++i)                                           \
        {                                                                      \
            system_f(Map<const Vector<n_x + n_u>>(traj_ptr + (n_x + n_u) * i), \
                     traj_ptr + (n_x + n_u) * (i + 1));                        \
        }                                                                      \
    }                                                                          \
                                                                               \
    void compute_from_DDP_output(                                              \
        const ndarray orig_traj, const Eigen::Ref<Vector<n_u>> lo,             \
        const Eigen::Ref<Vector<n_u>> hi,                                      \
        const std::vector<Eigen::Ref<Vector<n_u>>>& feedforward,               \
        const std::vector<Eigen::Ref<ColMatrix<n_u, n_x>>>& feedback,          \
        const double step, ndarray new_traj);                                  \
                                                                               \
    void compute_from_DDP_output_unbounded(                                    \
        const ndarray orig_traj,                                               \
        const std::vector<Eigen::Ref<Vector<n_u>>>& feedforward,               \
        const std::vector<Eigen::Ref<ColMatrix<n_u, n_x>>>& feedback,          \
        const double step, ndarray new_traj);                                  \
                                                                               \
    void compute_from_DDP_output(                                              \
        const ndarray orig_traj, const Eigen::Ref<Vector<n_u>> lo,             \
        const Eigen::Ref<Vector<n_u>> hi,                                      \
        const std::vector<Eigen::Ref<Vector<n_u>>>& feedforward,               \
        const std::vector<Eigen::Ref<ColMatrix<n_u, n_x>>>& feedback,          \
        const double step, ndarray new_traj)                                   \
    {                                                                          \
        auto n = orig_traj.shape(0) - 1;                                       \
        auto traj_ptr = orig_traj.data(0);                                     \
        auto traj_new_ptr = new_traj.mutable_data(0);                          \
                                                                               \
        std::copy(traj_ptr, traj_ptr + n_x, traj_new_ptr);                     \
        for (long i = 0; i < n; ++i)                                           \
        {                                                                      \
            Map<Vector<n_u>> u_new(traj_new_ptr + (n_x + i * (n_x + n_u)));    \
            u_new =                                                            \
                (Map<const Vector<n_u>>(traj_ptr + (n_x + i * (n_x + n_u))) +  \
                 step * feedforward[static_cast<size_t>(i)] +                  \
                 feedback[static_cast<size_t>(i)] *                            \
                     (Map<Vector<n_x>>(traj_new_ptr + i * (n_x + n_u)) -       \
                      Map<const Vector<n_x>>(traj_ptr + (i * (n_x + n_u)))))   \
                    .cwiseMin(hi)                                              \
                    .cwiseMax(lo);                                             \
            system_f(Map<const Vector<n_x + n_u>>(traj_new_ptr +               \
                                                  (i * (n_x + n_u))),          \
                     traj_new_ptr + (i + 1) * (n_x + n_u));                    \
        }                                                                      \
    }                                                                          \
    void compute_from_DDP_output_unbounded(                                    \
        const ndarray orig_traj,                                               \
        const std::vector<Eigen::Ref<Vector<n_u>>>& feedforward,               \
        const std::vector<Eigen::Ref<ColMatrix<n_u, n_x>>>& feedback,          \
        const double step, ndarray new_traj)                                   \
    {                                                                          \
        auto n = orig_traj.shape(0) - 1;                                       \
        auto traj_ptr = orig_traj.data(0);                                     \
        auto traj_new_ptr = new_traj.mutable_data(0);                          \
                                                                               \
        std::copy(traj_ptr, traj_ptr + n_x, traj_new_ptr);                     \
        for (long i = 0; i < n; ++i)                                           \
        {                                                                      \
            Map<Vector<n_u>> u_new(traj_new_ptr + (n_x + i * (n_x + n_u)));    \
            u_new =                                                            \
                Map<const Vector<n_u>>(traj_ptr + (n_x + i * (n_x + n_u))) +   \
                step * feedforward[static_cast<size_t>(i)] +                   \
                feedback[static_cast<size_t>(i)] *                             \
                    (Map<Vector<n_x>>(traj_new_ptr + i * (n_x + n_u)) -        \
                     Map<const Vector<n_x>>(traj_ptr + (i * (n_x + n_u))));    \
            system_f(Map<const Vector<n_x + n_u>>(traj_new_ptr +               \
                                                  (i * (n_x + n_u))),          \
                     traj_new_ptr + (i + 1) * (n_x + n_u));                    \
        }                                                                      \
    }                                                                          \
                                                                               \
    ndarray cost_sequence(const ndarray traj);                                 \
    ndarray cost_sequence(const ndarray traj)                                  \
    {                                                                          \
        auto n = traj.shape(0) - 1;                                            \
        auto traj_ptr = traj.data();                                           \
        ndarray costs(static_cast<size_t>(n + 1));                             \
        auto cost_ptr = costs.mutable_data();                                  \
        for (long i = 0; i < n; ++i)                                           \
        {                                                                      \
            *(cost_ptr + i) = system_l(Map<const Vector<n_x + n_u>>(           \
                traj_ptr + static_cast<size_t>(i) * (n_x + n_u)));             \
        }                                                                      \
        *(cost_ptr + n) = system_lf(Map<const Vector<n_x>>(                    \
            traj_ptr + static_cast<size_t>(n) * (n_x + n_u)));                 \
        return costs;                                                          \
    }                                                                          \
                                                                               \
    std::tuple<ndarray, ndarray> batch_f(const ndarray in)                     \
    {                                                                          \
        auto in_ptr = in.data(0);                                              \
        auto n = in.shape(0) - 1;                                              \
                                                                               \
        ndarray jacobian_out(                                                  \
            std::vector<size_t>{static_cast<size_t>(n), n_x, n_x + n_u});      \
        ndarray hessian_out(std::vector<size_t>{static_cast<size_t>(n), n_x,   \
                                                n_x + n_u, n_x + n_u});        \
                                                                               \
        auto jacobian_ptr = jacobian_out.mutable_data(0);                      \
        auto hessian_ptr = hessian_out.mutable_data(0);                        \
        _Pragma("omp parallel for") for (long i = 0; i < n; ++i)               \
        {                                                                      \
            hyperdual<(n_x + n_u)> f_out[n_x];                                 \
            auto f_in = from_vector<(n_x + n_u)>(in_ptr + ((n_x + n_u) * i));  \
            system_f(f_in, f_out);                                             \
            get_derivatives<(n_x + n_u), n_x>(f_out, jacobian_ptr,             \
                                              hessian_ptr, i);                 \
        }                                                                      \
        return std::make_tuple(jacobian_out, hessian_out);                     \
    }                                                                          \
                                                                               \
    std::tuple<ndarray, ndarray, ndarray, ndarray> batch_l(const ndarray in)   \
    {                                                                          \
        auto in_ptr = in.data(0);                                              \
        auto n = in.shape(0) - 1;                                              \
                                                                               \
        ndarray jacobian_out(                                                  \
            std::vector<size_t>{static_cast<size_t>(n), n_x + n_u});           \
        ndarray hessian_out(std::vector<size_t>{static_cast<size_t>(n),        \
                                                n_x + n_u, n_x + n_u});        \
                                                                               \
        ndarray fjacobian_out(std::vector<size_t>{n_x});                       \
        ndarray fhessian_out(std::vector<size_t>{n_x, n_x});                   \
                                                                               \
        auto jacobian_ptr = jacobian_out.mutable_data(0);                      \
        auto hessian_ptr = hessian_out.mutable_data(0);                        \
        auto fjacobian_ptr = fjacobian_out.mutable_data(0);                    \
        auto fhessian_ptr = fhessian_out.mutable_data(0);                      \
        _Pragma("omp parallel for") for (long i = 0; i < n; ++i)               \
        {                                                                      \
            const vec<hyperdual<(n_x + n_u)>, (n_x + n_u)> l_in =              \
                from_vector<(n_x + n_u)>(in_ptr + ((n_x + n_u) * i));          \
            auto l_out = system_l(l_in);                                       \
            get_derivatives<(n_x + n_u)>(l_out, jacobian_ptr, hessian_ptr, i); \
        }                                                                      \
                                                                               \
        const vec<hyperdual<n_x>, n_x> l_in =                                  \
            from_vector<n_x>(in_ptr + ((n_x + n_u) * n));                      \
        auto l_out = system_lf(l_in);                                          \
        get_derivatives<n_x>(l_out, fjacobian_ptr, fhessian_ptr, 0);           \
        return std::make_tuple(jacobian_out, hessian_out, fjacobian_out,       \
                               fhessian_out);                                  \
    }                                                                          \
                                                                               \
    PYBIND11_MODULE(module_name, m)                                            \
    {                                                                          \
                                                                               \
        m.def("f_derivs", &batch_f);                                           \
        m.def("l_derivs", &batch_l);                                           \
        m.def("solve", &compute_states);                                       \
        m.def("from_ddp_output", &compute_from_DDP_output);                    \
        m.def("from_ddp_output_unbounded",                                     \
              &compute_from_DDP_output_unbounded);                             \
        m.def("cost_seq", &cost_sequence);                                     \
    }
#endif /* ifndef HELPER */

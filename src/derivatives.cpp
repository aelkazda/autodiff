#include "derivatives.hpp"

Eigen::Vector3d reciprocal_derivative(double in)
{
    Eigen::Vector3d out;
    out[0] = 1.0 / in;
    out[1] = -out[0] * out[0];
    out[2] = -out[1] * out[0];
    return out;
}

template <> Eigen::Vector3d derivative<sin>(double in)
{
    Eigen::Vector3d out;
    double f0 = sin(in);
    out << f0, cos(in), -f0;
    return out;
}

template <> Eigen::Vector3d derivative<asin>(double in)
{
    Eigen::Vector3d out;
    double f0 = asin(in);
    double f1 = 1.0 / sqrt(1.0 - in * in);
    out << f0, f1, in / (f1 * f1 * f1);
    return out;
}

template <> Eigen::Vector3d derivative<cos>(double in)
{
    Eigen::Vector3d out;
    double f0 = cos(in);
    out << f0, -sin(in), -f0;
    return out;
}

template <> Eigen::Vector3d derivative<tan>(double in)
{
    Eigen::Vector3d out;
    double f0 = tan(in);
    double f1 = 1.0 + f0 * f0;
    out << f0, f1, f0 * f1;
    return out;
}

template <> Eigen::Vector3d derivative<sqrt>(double in)
{
    Eigen::Vector3d out;
    double f0 = sqrt(in);
    double f1 = 0.5 / f0;
    double f2 = -2.0 * f1 * f1 * f1;
    out << f0, f1, f2;
    ;
    return out;
}
